#Notas para los usuarios

## Compilación del programa

Se ejecuta la siguiente instrucción:

~~~~
make jar
~~~~

## Uso de la lista de vehículos

Permite ejecutar las instrucciones que se muestran a continuación:

1. Mostrar todos los vehículos en la lista: Se muestras todos los vehículos que hay en la lista.

~~~~
 java -jar Concesionario.jar show
~~~~

2. Mostrar ayuda: Se muestra esta misma guia pero de manera mas simplificada.

~~~~
java -jar Concesionario.jar help
~~~~

3. Añadir vehículo: Al annadir un vehículo este se guarda en la lista.

~~~~
java -jar Concesionrio.jar add <tipoDeVehiculo> <modelo> <marca> <caballos>
~~~~

    por ejemplo,
~~~~
java -jar Concesionario.jar add coche Rx45 Tesla 2000
~~~~

4. Borrar vehículo: Al borrar un vehículo lo eliminas de la lista.

~~~~
java -jar Concesionrio.jar remove <tipoDeVehiculo> <modelo> <marca> <caballos>
~~~~

    por ejemplo,
~~~~
java -jar Concesionario.jar remove coche Rx45 Tesla 2000
~~~~

5. Modificar vehículo: Al modificar cambias el primer vehículo introducido por el segundo vehiculo introducido.

~~~~
java -jar Concesionrio.jar modify <tipoDeVehiculo> <modelo> <marca> <caballos> <tipoDeVehiculo> <modelo> <marca> <caballos>
~~~~

    por ejemplo,
~~~~
java -jar Concesionario.jar modify coche Rx45 Tesla 2000 coche Rx20 Tesla 2100
~~~~

6. Generar .csv: Al crear el fichero csv podras ver una tabla en dormato csv del fichero listaVehiculo

~~~~
java -jar Concesionario.jar csv
~~~~



# Notas para los desarrolladores

## Generación de Javadoc

Se ejecuta la siguiete instrucción:

~~~~
make javadoc
~~~~
## Inspección de Javadoc

Suponiendo que tiene instalado `firefox`, se ejecuta:

~~~~
firefox html/index.html
~~~~

## Sobre el fichero _makefile_

Se han utilizado sentencias específicas de Linux, por tanto, sólo
ejecuta en este sistema operativo.


                                                                                                                                                                            1,1      Comienzo


