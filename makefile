jarfile = Concesionario.jar

jar: compile
	jar cvfm $(jarfile) Manifest.txt -C bin .
compile: limpiar
	find src -name "*.java" | xargs javac -cp bin -d bin 
javadoc: 
	rm -rf html
	mkdir html
	find src -name "*.java" | xargs javadoc -d html -encoding utf-8 -docencoding utf-8 -charset utf-8
limpiar:
	rm -rf bin
	rm -rf $(jarfile)
	mkdir bin
	rm -rf listaVehiculos.txt
	rm -rf listaVehiculo.csv

