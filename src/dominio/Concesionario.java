package dominio;

import java.util.ArrayList;
import java.io.IOException;
import java.io.File;
import java.util.Scanner;
import java.io.FileWriter;

/**
 * Esta clase define un objeto de contiene una lista de vehículos
 */


public class Concesionario{

	/**
	 * Atributos de la clase Concesionario
	 * -ArrayList <Vehiculo> listaVehiculos
	 * -String nombreFichero
	 * -String nombreFicheroCSV
	 */

	private ArrayList<Vehiculo> listaVehiculos = new ArrayList<>();
	private String nombreFichero = "listaVehiculos.txt";
	private String nombreFicheroCSV = "listaVehiculos.csv";


	/**
	 * Contructor de la clase Concesionario, inicia la lista metiendo los vehiculos que estan en el fichero en la libreta de vehículos
	 */

	public Concesionario(){
		FicheroALista();
	}

	/** Método que borra un vehiculo especificado de la lista de vehúclos
	 * @param vehiculoB El parametro vehiculo define el vehiculo que va a ser borrado de la lista
	 */

	public void borrarVehiculo(Vehiculo vehiculoB){

		boolean borrado = false;

		for(int i = listaVehiculos.size()-1; i >= 0; i--){
			if(listaVehiculos.get(i).toString().equalsIgnoreCase(vehiculoB.toString())){
				listaVehiculos.remove(i);
				borrado = true;
			}
		}
		if(borrado){
			System.out.println(vehiculoB.toString() + " se ha borrado");
		}else {
			System.out.println(vehiculoB.toString() + " No existe ese vehiculo en la lista");
		}
		listaAFichero();
	}

	/** Método que reemplaza un vehiculo especificado de la lista de vehículos con otro elegido
	 * @param vehiculoViejo El parametro vehiculoViejo define el vehiculo que va a ser modifica de la lista de vehículos
	 * @param vehiculoNuevo El parametro vehiculoNuevo define la modificacion del vehiculo de la lista de vehículos
	 */

	public void reemplazarVehiculo(Vehiculo vehiculoViejo, Vehiculo vehiculoNuevo){

		boolean modificado = false;

		for (int i = listaVehiculos.size()-1; i >= 0; i--){
			if(listaVehiculos.get(i).toString().equalsIgnoreCase(vehiculoViejo.toString())){
				listaVehiculos.set(i, vehiculoNuevo);
				modificado = true;
			}
		}
		if(modificado){
			System.out.println(vehiculoViejo.toString() + " Ha sido modificado por " + vehiculoNuevo.toString());
		}else {
			System.out.println(vehiculoViejo.toString() + " No existe ese vehiculo en la lista");
		}
		listaAFichero();
	}

	/**
	 *  Método que annade un vehiculo a la lista de vehículos
	 *  @param vehiculo El parametro vehiculo define el vehiculo que va a ser añadido a la lista de vehículos, ademas dependiendo del tipo de coche crea un objeto u otro a partir de herencia
	 */


	public void annadirVehiculo(Vehiculo vehiculo){

		if(vehiculo.getTipoDeVehiculo().equalsIgnoreCase("coche")){
			Coche coche = new Coche(vehiculo.getMarca(), vehiculo.getModelo(), vehiculo.getCaballos());
                        listaVehiculos.add(coche);
		} else {
			if (vehiculo.getTipoDeVehiculo().equalsIgnoreCase("camioneta")){
				Camioneta camioneta = new Camioneta(vehiculo.getMarca(), vehiculo.getModelo(), vehiculo.getCaballos());
                                listaVehiculos.add(camioneta);
	
			}else{
				if (vehiculo.getTipoDeVehiculo().equalsIgnoreCase("camion")){
                                	Camion camion = new Camion(vehiculo.getMarca(), vehiculo.getModelo(), vehiculo.getCaballos());
                                	listaVehiculos.add(camion);
				}else {
					System.out.println("[ERROR} Los unicos tipos de vehiculo pueden ser camioneta, camion y coche");
				}

			}
		}
		listaAFichero();
	}

	/**
	 *Método que muestra todos los vehículos que hay en la lista de vehículos
	 */

	public void mostrarVehiculos(){

		for(int f = 0; f < listaVehiculos.size(); f++){
			System.out.println(listaVehiculos.get(f));
		}
	}

	/**
	 * Método que mete todos los vehiculos presentes en la lista de vehículos en el fichero
	 */

	private void listaAFichero(){

		try{
			File fichero = new File(nombreFichero);
			FileWriter fw = new FileWriter(fichero);
			fw.write(this.toString());
			fw.close();
		}catch(IOException ex){
			System.err.println("No se ha podido escribir en el fichero");
		}
	}

	 /**
         * Método que mete todos los vehiculos presentes en la lista de vehículos en el fichero CSV
         */

        public void generarCSV(){

                try{
                        File fichero = new File(nombreFicheroCSV);
                        FileWriter fw = new FileWriter(fichero);
                        fw.write(this.toString());
                        fw.close();
                }catch(IOException ex){
                        System.err.println("No se ha podido escribir en el fichero");
                }
        }


	/**
	 * Método que mete todos los vehículos presentes en el fichero en la lista de vehículos
	 */

	private void FicheroALista(){

		try{
			File fichero = new File(nombreFichero);
			if (fichero.createNewFile()){
				System.out.println("Se ha creado un nuevo fichero");

			} else {
				Scanner sc = new Scanner(fichero);
				while(sc.hasNext()){
					listaVehiculos.add(new Vehiculo(sc.next(), sc.next(), sc.next(), Integer.parseInt(sc.next())));
				}
				sc.close();
			}
		}catch(IOException ex){
			System.out.println(ex);
		}
	}

	/**
	 * Método que retorna el toString de los vehículos de la lista de vehículos.
	 * @return El metodo retorna un String con todos los atributus de cada vehículo de la lista de vehículos
	 */

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(Vehiculo vehiculo : listaVehiculos) sb.append(vehiculo + "\n");
		return sb.toString();
	}
}

/*Copyright [2020] [Juan Grima Sanchez]

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/


