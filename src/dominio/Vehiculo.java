package dominio;

/**
 * Esta clase define el objeto vehiculo que vamos a utilizar en el ArrayList
 */

public class Vehiculo{

	/**
	 * Atributos de la clase Vehiculo
	 * -String tipoDeVehiculo
	 * -String modelo
	 * -String marca
	 * -int caballos
	 */

	protected String tipoDeVehiculo,modelo, marca;
	protected int caballos;
 	/*
	 * Contructor de la clase Vehiculo que incluye todos los atributos
	 */
	public Vehiculo(String tipoDeVehiculo, String modelo, String marca, int caballos){
		this.tipoDeVehiculo = tipoDeVehiculo;
		this.modelo = modelo;
		this.marca = marca;
		this.caballos = caballos;
	}
	/* 
	 * Segundo constructor de la clase Vehiculo que incluye todos los atributos excepto el tipo de vehículo
	 */
	public Vehiculo(String modelo, String marca, int caballos){
                this.modelo = modelo;
                this.marca = marca;
                this.caballos = caballos;
        }



	public void setTipoDeVehiculo(String tipoDeVehiculo){
		this.tipoDeVehiculo = tipoDeVehiculo;
	}

	public void setModelo(String modelo){
		this.modelo = modelo;
	}

	public void setMarca(String marca){
		this.marca = marca;
	}

	public void setCaballos(int caballos){
		this.caballos = caballos;
	}

	public String getTipoDeVehiculo(){
		return tipoDeVehiculo;
	}

	public String getModelo(){
		return modelo;
	}

	public String getMarca(){
		return marca;
	}

	public int getCaballos(){
		return caballos;
	}
	/*
	* @return retorna todos los atributos de la clase en forma de String
	*/
	@Override
	public String toString(){
		return tipoDeVehiculo + " " + modelo + " " + marca + " " + caballos;
	}

}
/*Copyright [2020] [Juan Grima Sanchez]

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/


