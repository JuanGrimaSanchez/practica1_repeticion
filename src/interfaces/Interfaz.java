package interfaces;

import dominio.Concesionario;
import dominio.Vehiculo;

/**
 * Esta clase es la interfaz del usuario, aqui es donde todos los metodos se organizan para crear el programa
 */


public class Interfaz{

	/**
	 * Atributos de la clase intefaz
	 * -Concesionario concesionario
	 */

        private static Concesionario concesionario = new Concesionario();

	/**
	 * Método auxiliar de ejecutar que siver para mostrar la guia para utilizar el programa
	 */

        public static void mostrarAyuda(){
                System.out.println("Los comandos a utilizar son los siguientes");
                System.out.println("1.- Mostrar informacion de todos los vehiculos del concesionario: java -jar Concesionario.jar show");
                System.out.println("2.- Mostrar esta ayuda: java -jar Concesionario.jar help");
                System.out.println("3.- Añadir vehiculo: java -jar Concesionario.jar add <tipo de vehiculo> <modelo> <marca> <caballos>");
                System.out.println("4.- Borrar vehiculo: java -jar Concesionario.jar remove <tipo de vehiculo> <modelo> <marca> <caballos>");
		System.out.println("1.- Generar fichero .csv: java -jar Concesionario.jar csv");
                System.out.println("5.- Modificar vehiculo: java -jar Concesionario.jar modify <tipo de vehiculo> <modelo> <marca> <caballos> <tipo de vehiculo> <modelo> <marca> <caballos>");
        }

	/**
         * Método que utiliza todos los métodos definidos en la clase Concesionario para darle vida al programa
         */

        public static void ejecutar(String[] args){
                try{
                        if(args[0].equalsIgnoreCase("add")){
                                concesionario.annadirVehiculo(new Vehiculo(args[1], args[2], args[3], Integer.parseInt(args[4])));
                        }else{
                                if(args[0].equalsIgnoreCase("show")){
                                        concesionario.mostrarVehiculos();
                                }else{
                                        if(args[0].equalsIgnoreCase("remove")){
                                                concesionario.borrarVehiculo(new Vehiculo(args[1], args[2], args[3], Integer.parseInt(args[4])));
                                        }else{
                                                if(args[0].equalsIgnoreCase("modify")){
                                                        concesionario.reemplazarVehiculo(new Vehiculo(args[1], args[2], args[3], Integer.parseInt(args[4])), new Vehiculo(args[5], args[6], args[7], Integer.parseInt(args[8])));
                                                }else{
                                                        if(args[0].equalsIgnoreCase("help")){
                                                                mostrarAyuda();
                                                        }else{
								if(args[0].equalsIgnoreCase("csv")){
									concesionario.generarCSV();
								}
							}
                                                }
                                        }
                                }
                        }


                }catch(Exception e){
                        mostrarAyuda();
                        System.out.println("Problema: ");
                        System.out.println(e);
                }
        }

}

/*Copyright [yyyy] [name of copyright owner]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.*/




